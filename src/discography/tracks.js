import parse from 'date-fns/parse'
import albums from './albums'

class Appearance {
	constructor({track, album, number, disc}) {
		this.track = track
		this.album = album
		this.number = number || this.album.appearances.length + 1
		this.disc = disc || 1
	}
}

class Track {
	constructor({title, date, length, disc}) {
		this.title = title
		this.date = parse(date, 'MMMM D, YYYY', new Date()) || date
		this.length = length
		this.disc = disc
		this.appearances = []
		this.links = []
	}
	appearsOn({albumTitle, number, disc}) {
		const album = albums().find(a => a.title === albumTitle)
		const appearance = new Appearance({track: this, album, number, disc})
		this.appearances.push(album)
		album.appearances.push(appearance)
		return this
	}
	addLinks(...links) {
		this.links.push(...links)
		return this
	}
}

let __tracks
const tracks = () =>
	__tracks
		? __tracks
		: (__tracks = [
				//
				// Maybe I'm Dreaming
				//
				new Track({title: 'On the Wing', length: '5:04'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({title: 'Rainbow Veins', length: '4:40'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({title: 'Super Honeymoon', length: '3:22'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({
					title: 'The Saltwater Room',
					length: '4:52',
				}).appearsOn({albumTitle: "Maybe I'm Dreaming"}),
				new Track({title: 'Early Birdie', length: '4:15'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({title: 'Air Traffic', length: '3:04'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({title: 'The Technicolor Phase', length: '4:27'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({title: 'Sky Diver', length: '2:48'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({title: 'Dear Vienna', length: '3:58'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({title: "I'll Meet You There", length: '4:16'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({title: 'This Is the Future', length: '2:51'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),
				new Track({title: 'West Coast Friendship', length: '4:03'}).appearsOn({
					albumTitle: "Maybe I'm Dreaming",
				}),

				//
				// Ocean Eyes
				//
				new Track({title: 'Cave In', length: '4:02'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'The Bird and the Worm', length: '3:27'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'Hello Seattle', length: '2:47'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'Umbrella Beach', length: '3:50'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'The Saltwater Room', length: '4:02'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'Dental Care', length: '3:11'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'Meteor Shower', length: '2:14'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'On the Wing', length: '5:01'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'Fireflies', length: '3:48'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'The Tip of the Iceberg', length: '3:23'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'Vanilla Twilight', length: '3:52'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),
				new Track({title: 'Tidal Wave', length: '3:10'}).appearsOn({
					albumTitle: 'Ocean Eyes',
				}),

				// D2
				new Track({title: 'Hot Air Balloon', length: '3:35'}).appearsOn({
					albumTitle: 'Ocean Eyes',
					disc: 2,
					number: 1,
				}),
				new Track({title: 'Butterfly Wings', length: '2:54'}).appearsOn({
					albumTitle: 'Ocean Eyes',
					disc: 2,
					number: 2,
				}),
				new Track({title: 'Rugs from Me to You', length: '1:27'}).appearsOn({
					albumTitle: 'Ocean Eyes',
					disc: 2,
					number: 3,
				}),
				new Track({title: 'Sunburn', length: '3:47'}).appearsOn({
					albumTitle: 'Ocean Eyes',
					disc: 2,
					number: 4,
				}),
				new Track({title: 'Hello Seattle (Remix)', length: '5:53'}).appearsOn({
					albumTitle: 'Ocean Eyes',
					disc: 2,
					number: 5,
				}),
				new Track({title: 'If My Heart Was a House', length: '4:06'}).appearsOn(
					{
						albumTitle: 'Ocean Eyes',
						disc: 2,
						number: 6,
					}
				),
				new Track({title: 'Strawberry Avalanche', length: '3:18'}).appearsOn({
					albumTitle: 'Ocean Eyes',
					disc: 2,
					number: 7,
				}),
				new Track({
					title: 'Fireflies (Adam Young Remix)',
					length: '3:12',
				}).appearsOn({albumTitle: 'Ocean Eyes', disc: 2, number: 8}),

				//
				// All Things Bright and Beautiful
				//
				new Track({title: 'The Real World', length: '3:34'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'Deer in the Headlights', length: '3:00'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'Angels', length: '3:40'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({
					title: "Dreams Don't Turn to Dust",
					length: '3:44',
				}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'Honey and the Bee', length: '3:44'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'Kamikaze', length: '3:27'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'January 28, 1986', length: '0:37'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'Galaxies', length: '4:03'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'Hospital Flowers', length: '3:39'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'Alligator Sky', length: '3:05'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'The Yacht Club', length: '4:32'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'Plant Life', length: '4:10'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({title: 'How I Became the Sea', length: '4:25'}).appearsOn({
					albumTitle: 'All Things Bright and Beautiful',
				}),
				new Track({
					title: 'Alligator Sky (No Rap version)',
					length: '4:25',
				}).appearsOn({albumTitle: 'All Things Bright and Beautiful'}),
				new Track({
					title: 'Lonely Lullaby',
					length: '4:28',
				}).appearsOn({albumTitle: 'All Things Bright and Beautiful'}),
				new Track({
					title: 'How I Became the Sea',
					length: '4:25',
				}).appearsOn({albumTitle: 'All Things Bright and Beautiful'}),
				new Track({
					title: 'Shy Violet',
					length: '3:49',
				}).appearsOn({albumTitle: 'All Things Bright and Beautiful'}),
				new Track({
					title: 'To the Sky',
					length: '3:40',
				}).appearsOn({albumTitle: 'All Things Bright and Beautiful'}),

				//
				// The Midsummer Station
				//
				new Track({title: 'Dreams and Disasters', length: '3:45'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Shooting Star', length: '4:07'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Gold', length: '3:56'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Dementia', length: '3:31'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: "I'm Coming After You", length: '3:30'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Speed of Love', length: '3:28'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Good Time', length: '3:26'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Embers', length: '3:45'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Silhouette', length: '4:12'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Metropolis', length: '3:39'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Take It All Away', length: '3:31'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Bombshell Blonde', length: '3:26'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),
				new Track({title: 'Top of the World', length: '3:30'}).appearsOn({
					albumTitle: 'The Midsummer Station',
				}),

				//
				// Mobile Orchestra
				//
				new Track({title: 'Mobile Orchestra', length: '0:35'}).appearsOn({
					albumTitle: 'Mobile Orchestra',
				}),
				new Track({
					title: 'Tokyo (feat. Sekai no Owari)',
					length: '3:39',
				}).appearsOn({albumTitle: 'Mobile Orchestra'}),
				new Track({
					title: 'Verge (feat. Aloe Blacc)',
					length: '3:33',
				}).appearsOn({
					albumTitle: 'Mobile Orchestra',
				}),
				new Track({title: 'I Found Love', length: '3:39'}).appearsOn({
					albumTitle: 'Mobile Orchestra',
				}),
				new Track({
					title: 'Thunderstruck (feat. Sarah Russell)',
					length: '4:07',
				}).appearsOn({albumTitle: 'Mobile Orchestra'}),
				new Track({title: 'My Everything', length: '3:45'}).appearsOn({
					albumTitle: 'Mobile Orchestra',
				}),
				new Track({
					title: 'Unbelievable (feat Hanson)',
					length: '3:13',
				}).appearsOn({
					albumTitle: 'Mobile Orchestra',
				}),
				new Track({title: 'Bird With a Broken Wing', length: '3:55'}).appearsOn(
					{
						albumTitle: 'Mobile Orchestra',
					}
				),
				new Track({
					title: 'Back Home (feat. Jake Owen)',
					length: '3:09',
				}).appearsOn({
					albumTitle: 'Mobile Orchestra',
				}),
				new Track({title: "Can't Live Without You", length: '3:11'}).appearsOn({
					albumTitle: 'Mobile Orchestra',
				}),
				new Track({
					title: "You're Not Alone (feat. Britt Nicole)",
					length: '3:54',
				}).appearsOn({albumTitle: 'Mobile Orchestra'}),
				new Track({title: "This Isn't the End", length: '3:23'}).appearsOn({
					albumTitle: 'Mobile Orchestra',
				}),

				//
				// TODO: Cinematic
				//
		  ])
export default tracks
