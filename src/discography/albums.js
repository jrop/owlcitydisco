import parse from 'date-fns/parse'

class Album {
	constructor({title, date, label, cover}) {
		this.title = title
		this.date = parse(date, 'MMMM D, YYYY', new Date()) || date
		this.label = label
		this.cover = cover
		this.appearances = []
	}
}

let __albums
const albums = () =>
	__albums
		? __albums
		: (__albums = [
				new Album({
					title: "Maybe I'm Dreaming",
					date: 'March 17, 2008',
					label: 'Sky Harbor',
					cover: require("./Maybe_I'm_Dreaming.jpg"),
				}),
				new Album({
					title: 'Ocean Eyes',
					date: 'July 14, 2009',
					label: 'Universal Republic',
					cover: require('./Ocean_Eyes.jpg'),
				}),
				new Album({
					title: 'All Things Bright and Beautiful',
					date: 'June 14, 2011',
					label: 'Universal Republic',
					cover: require('./All_Things_Bright_and_Beautiful.jpg'),
				}),
				new Album({
					title: 'The Midsummer Station',
					date: 'August 21, 2012',
					label: 'Universal Republic',
					cover: require('./The_Midsummer_Station.jpg'),
				}),
				new Album({
					title: 'Mobile Orchestra',
					date: 'July 10, 2015',
					label: 'Republic',
					cover: require('./Mobile_Orchestra.png'),
				}),
				new Album({
					title: 'Cinematic',
					date: 'June 1, 2018',
					label: 'Independent',
				}),

				new Album({
					title: 'The Best of Owl City',
					date: 'July 9, 2014',
					label: 'Universal Music Japan',
				}),
				new Album({
					title: 'Of June',
					date: 'September 8, 2007',
					label: 'Sky Harbor',
				}),
				new Album({
					title: 'Shooting Star',
					date: 'May 15, 2012',
					label: 'Universal Republic',
				}),
				new Album({
					title: 'The Midsummer Station - Acoustic EP',
					date: 'July 30, 2013',
					label: 'Republic',
				}),
				new Album({
					title: 'Ultraviolet',
					date: 'June 27, 2014',
					label: 'Republic',
				}),
				new Album({
					title: 'Reel 1',
					date: 'December 1, 2017',
					label: 'Independent',
				}),
				new Album({
					title: 'Reel 2',
					date: 'February 2, 2018',
					label: 'Independent',
				}),
				new Album({title: 'Reel 3', date: 'Q2 2018', label: 'Independent'}),
		  ])
export default albums
